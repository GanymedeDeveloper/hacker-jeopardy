using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.Data;
using System.Text;

public class SQLiteManager {

    private string connectionString;


    public SQLiteManager (string connectionString) { 
           this.connectionString = connectionString;
    }

    public void WriteClue(int Cat_ID, Clue clue)
    {
        int isDual;
        string sqlQuery;

        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query
        IDbCommand dbCmd = dbConnection.CreateCommand();
        if (clue.IsDual())
        {
            isDual = 1;
            DualClue dual = (DualClue)clue;
            sqlQuery = 
                string.Format(
                    "INSERT INTO Clues(Cat_ID, Clue1, Clue2, Answer, Difficulty, IsDual) VALUES(\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\")", 
                    Cat_ID, dual.GetClue1(), dual.GetClue2(), clue.GetAnswer(), clue.GetDifficulty(), isDual
                    );

        }
        else {
            isDual = 0;
            SingleClue single = (SingleClue)clue;
            sqlQuery = string.Format(
                "INSERT INTO Clues(Cat_ID, Clue1, Clue2, Answer, Difficulty, IsDual) VALUES(\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\")", 
                Cat_ID, single.GetClue(), null, clue.GetAnswer(), clue.GetDifficulty(), isDual
                );
        }

        UnityEngine.Debug.Log("before saving the clue into the database");
        // Execute the command
        dbCmd.CommandText = sqlQuery;
        dbCmd.ExecuteScalar();
        UnityEngine.Debug.Log("after saving the clue into the database");

        //get last clue ID
        dbConnection.Close();
        clue.SetID(getHighestCluesID());
        UnityEngine.Debug.Log(clue.GetID() + "ID CLue");
    }

    public void WriteCategorie(Category category)    // please use Categorys Id instead auto increment
    {

        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("INSERT INTO Categories(ID, Name, Discription) VALUES(\"{0}\",\"{1}\",\"{2}\")",
            category.getId(), category.getName(), category.getDescription());

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        dbCmd.ExecuteScalar();
        dbConnection.Close();
    }

    public void WriteGame(Game game)
    {
        // make string of the categories
        List<Category> categories = game.GetCategories();

        StringBuilder builder = new StringBuilder();

        foreach (Category cat in categories)
        {
            builder.Append(cat.getId().ToString()).Append(",");
        }

        string categoriesString = builder.ToString();
        if (!(categoriesString.Length == 0))
        {
            categoriesString = categoriesString.Remove(categoriesString.Length - 1);
        }


        // make string of the dailydoubles
        List<(int, int)> dailydoubles = game.GetDailyDoubles();

        StringBuilder builder2 = new StringBuilder();

        foreach ((int, int) dd in dailydoubles)
        {
            builder2.Append(dd.Item1).Append(",").Append(dd.Item2).Append("|");
        }

        string dailyDoublesString = builder2.ToString();
        if (!(dailyDoublesString.Length == 0))
        {
            dailyDoublesString = dailyDoublesString.Remove(dailyDoublesString.Length - 1);
        }

        // checking Double Jeopardy

        int DJ;

        if (game.IsDouble())
            DJ = 1;
        else
            DJ = 0;

        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
            dbConnection.Open();

        // get the final jeopardy clue
        Clue Finalclue = game.GetFinalJeopardyClue();
   
        // Make the connection a command and write a query
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format(
            "INSERT INTO Games(Name, Description, Categories, DailyDoubles, DoubleJeopardy, FinalJeopardy) VALUES(\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\")",
            game.GetGameName(),game.GetDescription(), categoriesString, dailyDoublesString,DJ, Finalclue.GetID()
            );

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        dbCmd.ExecuteScalar();
        dbConnection.Close();

    }

    /* Ninos Function */
    public Dictionary<int, Category> ReadCategories()
    {
        int i = 0;

        Dictionary<int,Category> Cat = new Dictionary<int,Category>();

        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = "SELECT * FROM Categories";

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();
        while (reader.Read())
        {

            Category c = new Category(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), ReadCluesOfCategory(reader.GetInt32(0)));

            Cat.Add(i,c);
            i++;
        }

        dbConnection.Close();

        return Cat;
    }


    public Dictionary<int, Clue> ReadCluesOfCategory(int C_ID)
    {

        int i = 0;

        Dictionary<int, Clue> clues = new Dictionary<int, Clue>();

        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM Clues WHERE Cat_ID = {0}", C_ID);

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();
        while (reader.Read())
        {
                DualClue   DC;
                SingleClue SC;
            if (reader.GetInt32(6) == 1)
                {
                    DC = new DualClue(reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
                    clues.Add(i, DC);
                }


           else
                {
                    SC = new SingleClue(reader.GetString(2), reader.GetString(4), reader.GetInt32(5));
                    clues.Add(i, SC);
                }

        

            i++;

        }

        dbConnection.Close();

        return clues;
    }


    /* Maher Function Get Games */
    public List<string> ReadGames()
    {

        List<string> games = new List<string>();

        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query

        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = "SELECT * FROM Games";

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();
        while (reader.Read())
        {
            games.Add(reader.GetString(1));
        }

        dbConnection.Close();

        return games;
    }

    public List<Game> ReadGamesAsObjects()
    {
        List<Game> games = new List<Game>();
        List<Category> categories = new List<Category>();
        List<(int, int)> DailyDoubles = new List<(int, int)>();
        Clue finalClue;
        bool dj = false;

        Dictionary<int, Clue> Clues = new Dictionary<int, Clue>();


        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = "SELECT * FROM Games";

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();
        while (reader.Read())
        {
         
            categories = ReadCategoriesOfGame(reader.GetString(1));

            int clue_ID = reader.GetInt32(6);
            finalClue = getFinalClue(clue_ID);

            int DJ = reader.GetInt32(5);
            if (DJ == 1)
                dj = true;

            DailyDoubles = ReadDailyDoublesOfGame(reader.GetString(1));

            Game game = new Game(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), categories, finalClue, dj, DailyDoubles);

            games.Add(game);
        }

        dbConnection.Close();

        return games;
    }

    public List<(int,int)> ReadDailyDoublesOfGame (string GameName)
    {
        List<(int,int)> dd = new List<(int,int)>();

        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("SELECT DailyDoubles FROM Games WHERE Name = '{0}'", GameName);

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();
        reader.Read();
        string AllDailyDoublesAsString = reader.GetString(0);

        // Parsing the string
        if (!string.IsNullOrWhiteSpace(AllDailyDoublesAsString))
        {
            string[] ddTuples = AllDailyDoublesAsString.Split('|');

            foreach (string ddString in ddTuples)
            {
                string[] items = ddString.Split(',');
                (int, int) tuple;
                tuple.Item1 = int.Parse(items[0]);  //or Int32.TryParse(value, out num);
                tuple.Item2 = int.Parse(items[1]);
                dd.Add(tuple);
            }
        }
        

        dbConnection.Close();
        return dd;
    }

    /* Maher Function */
    public List<Category> ReadCategoriesOfGame(string GameName)
    {
    
        List <Category> Cat = new List<Category>();

        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("SELECT Categories FROM Games WHERE Name = '{0}'", GameName);
        
        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();
        reader.Read();
        string AllCategoriesAsString = reader.GetString(0);

        // Parsing the string
        string[] words = AllCategoriesAsString.Split(',');

        foreach (string word in words)
        {
            
            int n = int.Parse(word);
            Category c = GetSpecificCategory(n);

            Cat.Add(c);

        }

        dbConnection.Close();
        return Cat;
    }

    public Category GetSpecificCategory(int id)
    {
        Category c;

        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query

        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM Categories WHERE ID = {0}", id);

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();
        reader.Read();
    
        c = new Category(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), ReadCluesOfCategory(reader.GetInt32(0)));


        dbConnection.Close();
        return c;
    }

    public Clue getFinalClue (int id)
    {
        Clue FC;

        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query

        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM Clues WHERE ID = {0}", id);

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();
        reader.Read();
        if (reader.GetInt32(6) == 1)
        {
            FC = new DualClue(reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
            
        }


        else
        {
            FC = new SingleClue(reader.GetString(2), reader.GetString(4), reader.GetInt32(5));
            
        }

        dbConnection.Close();
        return FC;
    }


    public int getHighestCluesID()
    {
         
        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query

        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = "SELECT ID FROM Clues where ID = (SELECT max(ID) FROM Clues)";

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();

        int num = 0;
        while (reader.Read())
        {
            num = reader.GetInt32(0);
        }
        dbConnection.Close();
        return num;
    }

    public bool IsCategoryTableEmpty()
    {
        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query

        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery1 = "SELECT COUNT(*) FROM Categories";
        dbCmd.CommandText = sqlQuery1;
        IDataReader reader = dbCmd.ExecuteReader();
        reader.Read();
        int num = reader.GetInt32(0);
        dbConnection.Close();

        return num == 0 ? true : false;
    }

    public int getHighestCategoriesID()
    {
        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();

        // Make the connection a command and write a query
        int num = 0;
        if (!this.IsCategoryTableEmpty())
        {
            IDbCommand dbCmd = dbConnection.CreateCommand();
            string sqlQuery = "SELECT MAX(ID) FROM Categories";
            dbCmd.CommandText = sqlQuery;
            IDataReader reader = dbCmd.ExecuteReader();
            reader.Read();
            num = reader.GetInt32(0);
        }
        
        dbConnection.Close();
        return num;
    }

    public void removeAllCategories()
    {
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("delete from Categories where 1=1");

        dbCmd.CommandText = sqlQuery;
        dbCmd.ExecuteReader();

        this.removeAllClues();
        dbConnection.Close();
    }

    public void removeAllClues()
    {
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("delete from Clues where 1=1");

        dbCmd.CommandText = sqlQuery;
        dbCmd.ExecuteReader();
        dbConnection.Close();
    }

    public void removeAllGames()
    {
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("delete from Games where 1=1");

        dbCmd.CommandText = sqlQuery;
        dbCmd.ExecuteReader();
        dbConnection.Close();
    }

    private List<int> getGameIDsContainingCat(int catID)
    {
        List<int> gamesIDs = new List<int>();

        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = "SELECT * FROM GAMES";
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();

        while (reader.Read())
        {
            string categoriesString = reader.GetString(3);
            string[] catIDs = categoriesString.Split(',');
            int n;
            foreach (string id in catIDs)
            {
                n = int.Parse(id);
                if (n == catID)
                {
                    gamesIDs.Add(reader.GetInt32(0));
                }
            }
        }

        dbConnection.Close();
        return gamesIDs;
    }

    private void removeGamesContainingRemovedCategory(int catID)
    {
        List<int> gamesIDs = this.getGameIDsContainingCat(catID);

        foreach (int ID in gamesIDs)
        {
            this.removeSpecificGame(ID);
        }
    }

    public void removeSpecificCategory(int categoryId)
    {
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("delete from Categories where ID = {0}", categoryId);

        dbCmd.CommandText = sqlQuery;
        dbCmd.ExecuteReader();
        dbConnection.Close();

        //remove corresponding Clues and Games
        this.removeSpecificClue(categoryId);
        this.removeGamesContainingRemovedCategory(categoryId);
    }

    public void removeSpecificClue(int categoryId)
    {
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("delete from Clues where Cat_ID = {0}", categoryId);

        dbCmd.CommandText = sqlQuery;
        dbCmd.ExecuteReader();
        dbConnection.Close();
    }

    public void removeSpecificGame(int gameId)
    {
        IDbConnection dbConnection = new SqliteConnection(connectionString);
        dbConnection.Open();
        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = string.Format("delete from Games where ID = {0}", gameId);

        dbCmd.CommandText = sqlQuery;
        dbCmd.ExecuteReader();
        dbConnection.Close();
    }
}