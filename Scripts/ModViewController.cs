using UnityEngine;
using UnityEngine.UI;

public class ModViewController : MonoBehaviour
{

    public Text mod_Category;
    public Text mod_Value;
    public Text mod_Answer;
    public GameObject wager;
    public GameObject answer;
    public GameObject soloVideoController;
    public GameObject dualVideoController1;
    public GameObject dualVideoController2;
    public int money_wagered;

    public void Prep_Clue(Clue clue, string cat, bool dailyJeo, bool doubleJeo)
    {
        mod_Category.text = cat;
        mod_Answer.text = clue.GetAnswer();    

        if (dailyJeo)
        {
            mod_Value.text = "$???";
            wager.SetActive(true);
            answer.SetActive(false);
        }
        else
        {
            mod_Value.text = "$" + clue.GetAward(doubleJeo).ToString();
            wager.SetActive(false);
            answer.SetActive(true);
            money_wagered = 0;
        }

        if (!clue.IsDual())
        {
            SingleClue c = (SingleClue)clue;
            if(c.GetClueType() == Clue_Type.VIDEO)
                soloVideoController.SetActive(true);
            else
                soloVideoController.SetActive(false);
        }
        else
        {
            DualClue c = (DualClue)clue;
            if (c.GetType1() == Clue_Type.VIDEO)
                dualVideoController1.SetActive(true);
            else
                dualVideoController1.SetActive(false);

            if (c.GetType2() == Clue_Type.VIDEO)
                dualVideoController2.SetActive(true);
            else
                dualVideoController2.SetActive(false);
        }
    }

    public void enter_Wager(string i)
    {
        if (i == null)
            return;
        money_wagered = int.Parse(i);
        mod_Value.text ="$"+ i ;
    }

    public void finished_Wagering()
    {
        wager.transform.GetChild(0).GetComponent<InputField>().text = "";
        if (money_wagered != 0)
        {
            wager.SetActive(false);
            answer.SetActive(true);
        }
    }

    public int GetMoneyWagered()
    {
        return money_wagered;
    }
}
