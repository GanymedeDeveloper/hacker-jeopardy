using UnityEngine;

public class ClueStep : Step
{

    GameObject button;
    GameObject activeClue;

    public ClueStep( GameObject button, GameObject activeClue)
    {
        this.button = button;
        this.activeClue = activeClue;
    }

    public override void Undo()
    {
        button.SetActive(true);
        for (int i = 0; i < activeClue.transform.childCount; i++)
            activeClue.transform.GetChild(i).gameObject.SetActive(false);
        activeClue.SetActive(false);
    }

    public override bool BoardMode()
    {
        return true;
    }
}
