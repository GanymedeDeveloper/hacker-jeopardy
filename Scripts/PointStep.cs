public class PointStep : Step
{

    MyPlayer player;
    int points;
   (int, int) clue;

    public PointStep(MyPlayer player, int points, (int,int) clue)
    {
        this.player = player;
        this.points = points;
        this.clue = clue;
    }

    public override void Undo()
    {
        player.AddPoints(-points);
        player.SetTurn(true);
    }

    public override bool BoardMode()
    {
        return false;
    }

    public (int, int) getOldClue()
    {
        return clue;
    }


}
