using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuPanelController : MonoBehaviour
{

    public Dropdown gameListObiect;
    public List<int> gameIDs;
    public int currentGame;


    public void Play_Demo()
    {
        SceneManager.LoadScene(1);
    }

    public void OnApplicationQuit()
    {
        Debug.Log("has quit game");
        Application.Quit();

   }

    public void On_Panel(GameObject obj)
    {
        obj.SetActive(true);
    }

    public void Off_Panel(GameObject obj)
    {
        obj.SetActive(false);
    }

    public void setVolume(float v)
    {

    }

    public void StartGamePanel()
    {

    }

    public void EditGame()
    {

    }

    public void LoadGame()
    {

    }

    public void PlayGame()
    {

    }
    public void setCurrentGame(int i)
    {
       
    }
}
