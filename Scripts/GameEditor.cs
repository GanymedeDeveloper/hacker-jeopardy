using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEditor : MonoBehaviour
{
    public GameObject gameCreatorPanel;
    public GameObject creatCategoryPanel;
    public GameObject CluePanel; 
    public GameObject categoryPrefab;
    public GameObject selectedCategoryPrefab;
    public GameObject categoryContainer;
    public GameObject selectedCategoryContainer;
    public InputField answerText;
    public InputField clueText1;
    public InputField clueText2;
    public InputField gameName; 
    public Text mediaPath_1;
    public Text mediaPath_2;
    public InputField categoryName;
    public InputField description;
    public Text warningTextClue;
    public Text warningTextCategory;
    private int nextCategoryID = 0;
    public int currentClueIndex;
    public Category currentCategory; 
    Dictionary<int, GameObject> selectedcategoryObjects = new Dictionary<int, GameObject>();
    Dictionary<int, GameObject> CategoriesInDataBase = new Dictionary<int, GameObject>();
    Dictionary<int, Category> categories = new Dictionary<int, Category>();
    SQLiteManager sql;
    Game game;
    public Text WarningTextGame;
    public Dropdown dropdown;
    public Dropdown chooseGameDropDown;
    List<Game> games = new List<Game>();
    private bool editFlag = false;
    private bool finalJeopardyFlag = false;
    private void Start()
    {
        
        sql = new SQLiteManager("URI=file:" + Application.dataPath + "/HackerDB.sqlite");

        ///////////////
        //this.InitGame();
        ///////////////
        

        this.nextCategoryID = sql.getHighestCategoriesID() + 1;    //this way we'll never have id 0 in Categories table
        this.LoadCategoriesFromDataBank();
        games = sql.ReadGamesAsObjects();
        LoadGamesName();
        Debug.Log("Number of categories in the DB: " + categories.Count);

        //sql.removeAllCategories();
        //sql.removeAllClues();
        //sql.removeAllGames();

    }


    public void CreateClue ()
    {
        if (CheckClue())
        {

            Clue clue;
           
            if (IsDualClue())
            {
                string str1; string str2;
                str1 = mediaPath_1.text == "" ? clueText1.text : mediaPath_1.text;
                str2 = mediaPath_2.text == "" ? clueText2.text : mediaPath_2.text;
                clue = new DualClue(str1, str2, answerText.text, currentClueIndex);
            }
            else
            {
                string str1 = mediaPath_1.text == "" ? clueText1.text : mediaPath_1.text;
                clue = new SingleClue(str1, answerText.text, currentClueIndex);
            }
            if (!this.finalJeopardyFlag)
            {

                if (currentCategory.getClues().ContainsKey(currentClueIndex))
                    currentCategory.getClues().Remove(currentClueIndex);
                currentCategory.getClues().Add(currentClueIndex, clue);
            }
            else
            {
                game.SetFinalJeopardyClue(clue);
            }
            CluePanel.SetActive(false);
        }
    }
    public void LoadClue()
    {
        ClearClue();

        Clue clue;
        if (finalJeopardyFlag)
        {
            if (game.GetFinalJeopardyClue() == null)
            {
                return;
            }
            else
            {
                clue = game.GetFinalJeopardyClue();
            }
        }else
        {
            if (!currentCategory.getClues().TryGetValue(currentClueIndex, out clue)) return;
        }

                this.answerText.text = clue.GetAnswer();
                if (clue.IsDual())
                {
                    DualClue dualclue = (DualClue)clue;
                    this.answerText.text = dualclue.GetAnswer();

                    if (dualclue.GetType1() == Clue_Type.TEXT) clueText1.text = dualclue.GetClue1(); else mediaPath_1.text = dualclue.GetClue1();
                    if (dualclue.GetType1() == Clue_Type.TEXT) clueText2.text = dualclue.GetClue2(); else mediaPath_2.text = dualclue.GetClue2();
                }
                else
                {
                    SingleClue singleclue = (SingleClue)clue;
                    if (singleclue.GetClueType() == Clue_Type.TEXT) clueText1.text = singleclue.GetClue(); else mediaPath_1.text = singleclue.GetClue();
                }
            
        
        
          
    }
    public void SaveCategory()
    {
        currentCategory.setName(this.categoryName.text);
        currentCategory.setDescription(this.description.text);
        ++this.nextCategoryID;

        int id = this.currentCategory.getId();     // save id locally  (call by value)


        if (!categories.ContainsKey(id))
        {

            if (CheckCategory())
            {


                GameObject go = Instantiate(categoryPrefab, categoryContainer.transform) as GameObject; // display Cats in database; 
                go.transform.parent = categoryContainer.transform;

                Toggle toggle = go.GetComponentInChildren<Toggle>();

                Text text = toggle.GetComponentInChildren<Text>();        // Set Categorys Name in the created Prefab
                text.text = currentCategory.getName();

                toggle.isOn = false;
                Button[] buttons = go.GetComponentsInChildren<Button>();
                Button edit = buttons[0];       // Edit Button
                Button remove = buttons[1];
                edit.onClick.AddListener(delegate { EditCategory(id); });
                remove.onClick.AddListener(delegate { DeleteCategory(id); });

                toggle.onValueChanged.AddListener(delegate { ShowSelectedCategory(id, toggle); });

                categories.Add(id, currentCategory);
                CategoriesInDataBase.Add(id, go);

                this.creatCategoryPanel.SetActive(false);

                foreach (Clue c in currentCategory.getClues().Values)
                {
                    sql.WriteClue(id, c);
                }
                sql.WriteCategorie(currentCategory);
            }
        }
        else
        {
            --this.nextCategoryID;
            if (CheckCategory()) this.creatCategoryPanel.SetActive(false);
        }
       

        

    }

    public void ShowSelectedCategory(int id , Toggle toggle)

    {
       
        int selectedCats = this.selectedcategoryObjects.Count;
       
        if (toggle.isOn && selectedCats<6)        // by activate the toggle and if your selected categories are less than 6
        {
            ++selectedCats;
            
        
            GameObject go = Instantiate(selectedCategoryPrefab, selectedCategoryContainer.transform) as GameObject;
            this.selectedcategoryObjects.Add(id, go);
            go.transform.parent = selectedCategoryContainer.transform;

            Text t = go.GetComponentInChildren<Text>();
           
            this.categories.TryGetValue(id , out this.currentCategory);
            t.text = currentCategory.getName();
            int i = 0; 
            foreach (Button b in go.GetComponentsInChildren<Button>())
            {
                int clueIndex = i;
                b.onClick.AddListener(delegate { SetDailyDouble(id, clueIndex, b); });

                if (this.game!=null)
                {
                    (int, int) tuple = (this.game.GetIndexFromId(id), clueIndex);
                    if (game.IsDailyDouble(tuple))
                    {
                        Debug.Log("is daily double: " + tuple + " " + game.IsDailyDouble(tuple));
                        b.image.color = new Color(0, 1, 0, 1);
                    }
                }
                i++;
            }
            i = 0;
            if (!editFlag)
            {
                game.AddCat(categories[id]);
            }
            
        }
        else
        {
            if (!toggle.isOn && selectedCats > 0)        // by deactivate the toggle and if your selected catagories are more than zero
            {
                //  delete daily double 
              

                --selectedCats;

                GameObject objj;
                this.selectedcategoryObjects.TryGetValue(id, out objj);
                Destroy(objj);
                this.selectedcategoryObjects.Remove(id);
                game.RemoveCategory(categories[id]);
                game.RemoveDailyDouble();
                foreach(GameObject go in this.selectedcategoryObjects.Values)
                {
                    foreach(Button b in go.GetComponentsInChildren<Button>())
                    {
                        b.image.color = new Color(1, 1, 1, 1);
                    }
                }

            }
            else
            {
                Debug.Log("you have already selected 6 categories , please remove one to add another category");
                toggle.isOn = false; 
            }
        }
      
    }

    public void EditCategory(int id)
    {
        categories.TryGetValue(id, out this.currentCategory);
        this.warningTextCategory.text = "";
        this.categoryName.text = currentCategory.getName();
        this.description.text = currentCategory.getDescription();
        creatCategoryPanel.SetActive(true);

    }


    public void SaveGame()
    {
        game.SetGameName(this.gameName.text);
        if (CheckGame())
        {
            
            if (editFlag)
            {
                this.sql.WriteClue(-1, game.GetFinalJeopardyClue());
                this.sql.removeSpecificGame(this.game.GetID());
                this.sql.WriteGame(this.game);
                gameCreatorPanel.SetActive(false);
            }
            else
            {
                this.sql.WriteClue(-1, game.GetFinalJeopardyClue());
                this.sql.WriteGame(this.game);
                games.Add(this.game);
                dropdown.options.Add(new Dropdown.OptionData(this.game.GetGameName()));
                gameCreatorPanel.SetActive(false);
            }
            editFlag = false;
        }

    }
    public void LoadGame()
    {
        InitGame();
        if (dropdown.value >= 1)
        { 
            Debug.Log(game.GetDailyDoubles().Count + "Daily Double");
            int id = dropdown.value-1;
            this.game = games[id];
            Debug.Log(game.GetGameName() + " dailyD: " + game.GetDailyDoubles().Count);
            this.gameName.text = game.GetGameName();
            foreach (Category c in game.GetCategories())
            {
                GameObject obj = CategoriesInDataBase[c.getId()];
                Toggle toggle = obj.GetComponentInChildren<Toggle>();
                toggle.isOn = true;
            }
            gameCreatorPanel.SetActive(true);
        }
      
    }

    public void InitCategory()
    {
        currentCategory= new Category();
        currentCategory.setId(this.nextCategoryID);
        this.categoryName.text = "";
        this.description.text = "";
        this.warningTextCategory.text = "";
        
    }
    public void InitGame()
    {
        game = new Game();
        Debug.Log("DailyDuobles number: " + game.GetDailyDoubles().Count);
        ClearGame();
    }
    public void setCurrentClueIndex(int i)
    {
        this.currentClueIndex = i;
    }

    private bool IsDualClue()
    {
        if ((this.clueText1.text != "" || this.mediaPath_1.text != "") && (this.clueText2.text != "" || this.mediaPath_2.text != "")) return true;
        else return false; 
    }
    private void ClearClue ()
    {
        this.answerText.text = "";
        this.clueText1.text = "";
        this.clueText2.text = "";
        this.mediaPath_1.text = "";
        this.mediaPath_2.text = "";
        this.warningTextClue.text = ""; 

    }
    private void ClearGame()
    {
        this.gameName.text = "";
        this.WarningTextGame.text = "";
       
        foreach (int i in CategoriesInDataBase.Keys)
        {
            CategoriesInDataBase[i].GetComponentInChildren<Toggle>().isOn = false;
        }
        ClearClue();
        selectedcategoryObjects.Clear();
      
    }

    private void SetDailyDouble(int catId, int clueIndex,Button b)
    {
        Debug.Log("EditFlag : " + this.editFlag);
        Debug.Log("CatId : " + catId + "ClueIndex : " + clueIndex);
        (int, int) tuple = (game.GetIndexFromId(catId), clueIndex);
        Debug.Log("CatIndex : " + tuple.Item1 + "ClueIndex : " + tuple.Item2);
        if (game.IsDailyDouble(tuple))
        {
            
            b.image.color = new Color(1, 1, 1, 1);
            game.ResetDailyJeopardy(tuple);
        }
        else
        {
            
            b.image.color = new Color(0, 1, 0, 1);
            game.SetDailyDouble(tuple);
        }
        
    }

    public void LoadCategoriesFromDataBank()
    {
        Dictionary<int, Category> cats = sql.ReadCategories();
        foreach (Category cat in cats.Values)
        {
            GameObject go = Instantiate(categoryPrefab, categoryContainer.transform) as GameObject; // display Cats in database; 
            go.transform.parent = categoryContainer.transform;

            Toggle toggle = go.GetComponentInChildren<Toggle>();

            Text text = toggle.GetComponentInChildren<Text>();        // Set Categorys Name in the created Prefab
            text.text = cat.getName();

            toggle.isOn = false;

            toggle.onValueChanged.AddListener(delegate { ShowSelectedCategory(cat.getId(), toggle); });
            Button[] buttons = go.GetComponentsInChildren<Button>();
            Button edit = buttons[0];       // Edit Button
            Button remove = buttons[1];
            edit.onClick.AddListener(delegate { EditCategory(cat.getId()); });
            remove.onClick.AddListener(delegate { DeleteCategory(cat.getId()); });
            categories.Add(cat.getId(), cat);
            CategoriesInDataBase.Add(cat.getId(), go);
        }
    }
    private void LoadGamesName()
    {

        foreach (Game g in games)
        {
            this.dropdown.options.Add(new Dropdown.OptionData(g.GetGameName()));
            this.chooseGameDropDown.options.Add(new Dropdown.OptionData(g.GetGameName()));

        }
    }

    public void DeleteGame()
    {
      
        if (dropdown.value > 0) {
            sql.removeSpecificGame(games[dropdown.value - 1].GetID());                            //Error massage : DataBase is locked 
            dropdown.options.RemoveAt(dropdown.value);
            dropdown.RefreshShownValue();
        }

        if (this.chooseGameDropDown.value > 0)
        {
            this.chooseGameDropDown.options.RemoveAt(this.chooseGameDropDown.value);
            this.chooseGameDropDown.RefreshShownValue();
        }

    }
    public void DeleteCategory(int id)
    {
      Destroy(this.CategoriesInDataBase[id]);
      this.CategoriesInDataBase.Remove(id);
      sql.removeSpecificCategory(id);                                                               //Error massage : DataBase is locked 
    }

    public void SetFinalJeopardyFlag(bool b)
    {
        this.finalJeopardyFlag = b;
    }
    public void SetEditFlag()
    {
        editFlag = true;
    }
    private bool CheckClue()
    {
        if (string.IsNullOrWhiteSpace(this.answerText.text))
        {
            this.warningTextClue.text = "Enter an answer please";
            return false;
        }

        if ((string.IsNullOrWhiteSpace(this.clueText1.text) || !string.IsNullOrWhiteSpace(this.mediaPath_1.text)) &&
            (!string.IsNullOrWhiteSpace(this.clueText1.text) || string.IsNullOrWhiteSpace(this.mediaPath_1.text)))
        {

            warningTextClue.text = "Fill the first clue by adding a text or uploading a mediaFile please (You can not fill both)";
            return false;
        }
        else
        {
            if (!string.IsNullOrWhiteSpace(this.clueText2.text) && !string.IsNullOrWhiteSpace(this.mediaPath_2.text))
            {
                warningTextClue.text = "You can ether enter a text or a mediafile (Optional) ";
                return false;
            }
            else return true;
        }


    }
    private bool CheckCategory()
    {
        if (string.IsNullOrWhiteSpace(this.categoryName.text))
        {
            warningTextCategory.text = "fill the Category Name field please";
            return false;
        }
        else if (string.IsNullOrWhiteSpace(this.description.text))
        {
            warningTextCategory.text = "fill the description field please";
            return false;
        }
        else if (this.currentCategory.getClues().Count < 5)
        {
            warningTextCategory.text = "fill all 5 clues please";
            return false;
        }
        else return true;

    }
    private bool CheckGame()
    {
        if (game.getCategoriesNum() < 2 || game.getCategoriesNum() > 6)
        {
            this.WarningTextGame.text = "Select between 2 to 6 categories"; return false;
        }
        if (string.IsNullOrWhiteSpace(game.GetGameName()))
        {
            this.WarningTextGame.text = "Enter the Game Name Please";
            return false;
        }
        else
        {
            if (game.GetFinalJeopardyClue() == null)
            {
                this.WarningTextGame.text = "Enter finaljeopardy clue please"; return false;
            }
            else return true;

        }

    }
    public void CancelGamePanel()
    {
        editFlag = false;
        gameCreatorPanel.SetActive(false);
    }
}

