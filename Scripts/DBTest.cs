using Mono.Data.Sqlite;
using System.Data;
using UnityEngine;

public class DBTest : MonoBehaviour
{
    SQLiteManager sql;
    // Start is called before the first frame update
    void Start()
    {
        sql = new SQLiteManager("URI=file:" + Application.dataPath + "/TestDB.sqlite");
    }

    public void TestWriteClue()
    {
        Clue c = new SingleClue("mdasod","jes",2);
        sql.WriteClue(4,c);
    }
    /*
    public void TestWriteCategory()
    {
        Category c = new Category(1, "C++", "A programming langauge", null);
        sql.WriteCategorie(c);
    }

    public void TestWriteGame()
    {
        Game g = new Game(100, "The test Game", "This game is just a test", null, null, false, null);
        sql.WriteGame(g);
    }*/

    public void TestReadClue()
    {
        // Open a connection with the database
        IDbConnection dbConnection = new SqliteConnection("URI=file:" + Application.dataPath + "/HackerDB.sqlite");
        dbConnection.Open();

        // Make the connection a command and write a query

        IDbCommand dbCmd = dbConnection.CreateCommand();
        string sqlQuery = "SELECT * FROM Clues";

        // Execute the command
        dbCmd.CommandText = sqlQuery;
        IDataReader reader = dbCmd.ExecuteReader();
        while (reader.Read())
        {
            Debug.Log(reader.GetInt32(0).ToString() + reader.GetInt32(1).ToString() + reader.GetString(2) + reader.GetString(3) + reader.GetString(4) + reader.GetInt32(5).ToString() + reader.GetInt32(6).ToString());
        }

        dbConnection.Close();
    }



}

   
