using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    List<Step> steps = new List<Step>();
    GameSetup gameSetup;
    GameObject active_Now;
    ((int,int),Clue) active_Clue;
    
    public bool moderator_Mode = false;
    public Game game;
    public GameObject player_Prefab;
    public List<GameObject> score_Locations;
    public GameObject soloClueTemplate;
    public GameObject dualClueTemplate;
    public MultiScreenController screen_Controller;
    public ModViewController mod_Controller;

    //final Jeopardy objects
    public GameObject FinalJeopardyModPanel;
    public GameObject FinalJeopardyPlayerPanel;
    public GameObject FinalClueModPanel;
    public List<GameObject> RightWrongButtons;
    private int remClues = 0;
    private List<int> wagers = new List<int>();

    //final score display
    public GameObject ScoresModPanel;
    public GameObject ScoresPlayerPanel;


    private void Start()
    {
        steps = new List<Step>();

        gameSetup = gameObject.GetComponent<GameSetup>();
        //Initialising the demo game
        List<Category> cats = new List<Category>();
        Dictionary<int, Clue> clues = new Dictionary<int, Clue>();
        clues.Add(0, new SingleClue("What is a set of steps that create an ordered approach to a problem solution?", "Algorithm.", 1));
        clues.Add(1, new SingleClue("Name one of the methods to writing an algorithm.", "Plain English/Pseudocode/Flowchart.", 2));
        clues.Add(2, new SingleClue(Application.dataPath + "/Video_Clues/avenger.mp4", "how to do it.",  3));
        clues.Add(3, new DualClue("1. Leave classroom. 2. Turn right out of school building. 3. Walk 1.2 miles. 4. Turn right on street. 5. Go to 4th house.",Application.dataPath + "/Image_Clues/d.jpg", "This is an example of an algorithm.", 4));
        clues.Add(4, new DualClue("If the user does not provide the correct data into an algorithm, then you can use this to restart from a previous step.", Application.dataPath + "/Video_Clues/avenger.mp4", "Loop.", 5));




        /*
        Dictionary<int, Clue> clues2 = new Dictionary<int, Clue>();
        clues2.Add(0, new Clue(null, Application.dataPath + "/Image_Clues/a.jpg", null, 1, "tiger", true));
        clues2.Add(1, new Clue("a very fine tool for measuring, transferring, or injecting very small quantities of liquid.", null, null, 2, "What is pipette?", false));
        clues2.Add(2, new Clue("a chemical bond that involves the sharing of electron pairs between atoms.", null, null, 3, "What is a covalent bond?", false));
        clues2.Add(3, new Clue("the process whereby biological cells generate new __; it is balanced by the loss of cellular __ via degradation or export.", null, null, 4, "What is protein synthesis?", true));
        clues2.Add(4, new Clue("This type of mutation is a change in one DNA base pair that results in the substitution of one amino acid for another in the protein made by a gene.", null, null, 5, "What is Missense Mutation ?", false));

        categoriess.Add(3, new Category(4, "Science", "Random Science quizes", clues2));

        Dictionary<int, Clue> clues3 = new Dictionary<int, Clue>();
        clues3.Add(0, new Clue("This game series features the triforce of courage.", Application.dataPath + "/Image_Clues/c.jpg", null, 1, "The legend of Zelda", false));
        clues3.Add(1, new Clue("Considered by many to be the hardest Mario Game.", null, null, 2, "What is Super Mario Sunshine?", true));
        clues3.Add(2, new Clue("This game sold so poorly that in order to cut their losses, the company buried copies in the desert", null, null, 3, "What is ET: the game?", false));
        clues3.Add(3, new Clue("Despite a multitude of award-winning games, this nintendo console was considered a financial failure.", null, null, 4, "What is the Nintendo Game Cube?", false));
        clues3.Add(4, new Clue(null, Application.dataPath + "/Image_Clues/d.jpg", null, 5, "What is Dance Dance Revolution: Hottest Party?", true));

        categoriess.Add(2, new Category(5, "Video Games", "FUN", clues3));


        Dictionary<int, Clue> clues4 = new Dictionary<int, Clue>();
        clues4.Add(0, new Clue("The first film in the Marvel Cinematic Universe", null, null, 1, "What is Iron Man?", false));
        clues4.Add(1, new Clue("This comic event pitted the Marvel heroes against each other in a moral conflict, and was later adapted into a movie.", Application.dataPath + "/Image_Clues/e.jpg", null, 2, "What is Civil War ?", false));
        clues4.Add(2, new Clue("Chris Pratt plays this character on the big screen.", null, null, 3, "Who is Star Lord?", false));
        clues4.Add(3, new Clue("The first actor to play the Hulk", null, null, 4, "Who is Lou Ferrigno?", false));
        clues4.Add(4, new Clue(null, null, Application.dataPath + "/Video_Clues/avenger.mp4", 5, "What is Avengers EndGame?", true));

        categoriess.Add(1, new Category(3, "Marvel", "Biggest Superheros franchise", clues4));
        */

        cats.Add(new Category(0, "Algorithms", "Dealing with cool algorithms", clues));
        cats.Add(new Category(1, "Algorithms", "Dealing with cool algorithms", clues));
        cats.Add(new Category(2, "Algorithms", "Dealing with cool algorithms", clues));
        cats.Add(new Category(3, "Algorithms", "Dealing with cool algorithms", clues));
        cats.Add(new Category(4, "Algorithms", "Dealing with cool algorithms", clues));
        cats.Add(new Category(4, "Algorithms", "Dealing with cool algorithms", clues));

        List<(int, int)> lelist = new List<(int, int)>();
        lelist.Add((0, 0));
        lelist.Add((3, 3));

        game = new Game(0, "demo", "just a test", cats, new SingleClue("Finale", "true.", 1), true, lelist);
        FillClues(game);
        

        

        //FillClues(categoriess);

        
    }

    private void Update()
    {
        //get the players input
        if (!moderator_Mode)
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                game.GetPlayer(0).SetTurn(true);
                moderator_Mode = true;
            }
            else if (Input.GetKeyDown(KeyCode.F2))
            {
                game.GetPlayer(1).SetTurn(true);
                moderator_Mode = true;
            }

            if (game.GetPlayerCount() > 2)
            {
                if (Input.GetKeyDown(KeyCode.F3))
                {
                    game.GetPlayer(2).SetTurn(true);
                    moderator_Mode = true;
                }

                if(game.GetPlayerCount() > 3)
                {
                    if (Input.GetKeyDown(KeyCode.F4))
                    {
                        game.GetPlayer(3).SetTurn(true);
                        moderator_Mode = true;
                    }
                }
            }
        }
    }

     public void FillClues(Game game)
     {
        this.game = game;
        this.remClues = game.GetCatCount() * 5;
        gameSetup.Setup(game);

    }


    public void DisplayClue(int cat, int index, GameObject button, bool isFinal)
    {
        Clue clue;
        if (!isFinal)
            clue = game.GetCatAt(cat).getClue(index);
        else
            clue = game.GetFinalJeopardyClue();

        active_Clue = ((cat,index),clue);

        if (!clue.IsDual())
        {
            SingleClue c = (SingleClue) clue;
            soloClueTemplate.SetActive(true);
            active_Now = soloClueTemplate;
            switch (c.GetClueType())
            {

                case Clue_Type.TEXT:
                    GameObject text_Obj = soloClueTemplate.transform.GetChild(0).gameObject;
                    text_Obj.SetActive(true);
                    Text_Clue_Chosen(c.GetClue(), text_Obj);
                    break;

                case Clue_Type.IMAGE:
                    GameObject image_Obj = soloClueTemplate.transform.GetChild(1).gameObject;
                    image_Obj.SetActive(true);
                    Image_Clue_Chosen(c.GetClue(), image_Obj);
                    break;

                case Clue_Type.VIDEO:
                    GameObject video_Obj = soloClueTemplate.transform.GetChild(2).gameObject;
                    video_Obj.SetActive(true);
                    Video_Clue_Chosen(c.GetClue(), video_Obj);
                    break;

            }
        }
        else
        {
            DualClue c = (DualClue)clue;
            dualClueTemplate.SetActive(true);
            dualClueTemplate.transform.GetChild(3).gameObject.SetActive(true);
            active_Now = dualClueTemplate;
            switch (c.GetType1())
            {

                case Clue_Type.TEXT:
                    GameObject text_obj1 = dualClueTemplate.transform.GetChild(0).gameObject;
                    text_obj1.SetActive(true);
                    Text_Clue_Chosen(c.GetClue1(), text_obj1);
                    break;

                case Clue_Type.IMAGE:
                    GameObject image_obj1 = dualClueTemplate.transform.GetChild(1).gameObject;
                    image_obj1.SetActive(true);
                    Image_Clue_Chosen(c.GetClue2(), image_obj1);
                    break;

                case Clue_Type.VIDEO:
                    GameObject video_obj1 = dualClueTemplate.transform.GetChild(2).gameObject;
                    video_obj1.SetActive(true);
                    Video_Clue_Chosen(c.GetClue2(), video_obj1);
                    break;

            }

            switch (c.GetType2())
            {

                case Clue_Type.TEXT:
                    GameObject text_obj2 = dualClueTemplate.transform.GetChild(4).gameObject;
                    text_obj2.SetActive(true);
                    Text_Clue_Chosen(c.GetClue2(), text_obj2);
                    break;

                case Clue_Type.IMAGE:
                    GameObject image_obj2 = dualClueTemplate.transform.GetChild(5).gameObject;
                    image_obj2.SetActive(true);
                    Image_Clue_Chosen(c.GetClue2(), image_obj2);
                    break;

                case Clue_Type.VIDEO:
                    GameObject video_obj2 = dualClueTemplate.transform.GetChild(6).gameObject;
                    video_obj2.SetActive(true);
                    Video_Clue_Chosen(c.GetClue2(), video_obj2);
                    break;
            }
        }

        screen_Controller.Screen_Switcher();

        if (!isFinal)
        {
            steps.Add(new ClueStep(button, active_Now));
            mod_Controller.Prep_Clue(clue, game.GetCatAt(cat).getName(), game.IsDailyDouble((cat, index)), game.IsDouble());
        }
        else
            mod_Controller.Prep_Clue(clue, game.GetFinalJeopardyCat(), false, game.IsDouble());

        this.remClues--;
    }

    private void Text_Clue_Chosen(string c, GameObject guiObject)
    {
        guiObject.SetActive(true);
        guiObject.GetComponentInChildren<Text>().text = c;
    }

    private void Image_Clue_Chosen(string c, GameObject guiObject)
    {
        guiObject.SetActive(true);
        IEnumerator i = loadImage(guiObject, c);
        StartCoroutine(i);
    }

    IEnumerator loadImage(GameObject img_Object, string new_Image)
    {
        WWW www = new WWW(new_Image);
        while (!www.isDone)
            yield return null;
        img_Object.GetComponent<RawImage>().texture = www.texture;
        img_Object.GetComponent<ImageScaling>().Scale_Image();
    }

    private void Video_Clue_Chosen(string c, GameObject guiObject)
    {
        guiObject.SetActive(true);
        IEnumerator i = loadVideo(guiObject, c);
        StartCoroutine(i);
    }

    IEnumerator loadVideo(GameObject vid_Object, string new_Video)
    {
        VideoPlayer vid_Player = vid_Object.GetComponent<VideoPlayer>();
        vid_Player.url = new_Video;
        vid_Player.Prepare();
        while (!vid_Player.isPrepared)
            yield return null;

        vid_Player.Play();
        vid_Object.GetComponent<VideoScaling>().Scale_Video();
    }

    public void Undo()
    {
        Step s = steps[steps.Count - 1];
        s.Undo();
        if (s.BoardMode())
        {
            moderator_Mode = !moderator_Mode;
            for (int i = 0; i < game.GetPlayerCount(); i++)
            {
                game.GetPlayer(i).SetTurn(false);
            }
            screen_Controller.Screen_Switcher();
        }
        else
        {
            PointStep p = (PointStep)s;
            moderator_Mode = true;
            DisplayClue(p.getOldClue().Item1, p.getOldClue().Item2, null, false);
            steps.Remove(steps[steps.Count - 1]);
        }
        steps.Remove(s);

    }

    public void Rate(bool rate)
    {
        
        for(int i = 0; i < game.GetPlayerCount(); i++)
        {
            MyPlayer player = game.GetPlayer(i);
            if (player.IsUp())
            {
                int points = 0;
                if (rate)
                {
                    if (mod_Controller.money_wagered == 0)
                        points = active_Clue.Item2.GetAward(game.IsDouble());
                    else
                        points = mod_Controller.GetMoneyWagered();
                }
                else
                {
                    if (mod_Controller.money_wagered == 0)
                        points = -active_Clue.Item2.GetAward(game.IsDouble());
                    else
                        points = -mod_Controller.GetMoneyWagered();
                }
                player.AddPoints(points);
                steps.Add(new PointStep(player, points, active_Clue.Item1));
                player.SetTurn(false);
                break;
            }
        }


        for ( int i = 0; i < active_Now.transform.childCount; i++)
            active_Now.transform.GetChild(i).gameObject.SetActive(false);

        active_Now.SetActive(false);

        moderator_Mode = false;
        screen_Controller.Screen_Switcher();

        checkFinalJeopardy();

    }

    private void checkFinalJeopardy()
    {
        if (this.remClues == (game.getCategoriesNum() * 5) - 3)
        {
            FinalJeopardyModPanel.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Category: " + game.GetFinalJeopardyCat();
            FinalJeopardyPlayerPanel.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = game.GetFinalJeopardyCat();
            GameObject finalWagers = FinalJeopardyModPanel.transform.GetChild(0).gameObject;
            List<MyPlayer> removedPlayers = new List<MyPlayer>();
            //Remove bad players
            for (int index = 0; index < game.GetPlayerCount(); index++)
            {
                MyPlayer pl = game.GetPlayer(index);
                if (pl.GetPoints() <= 0)
                {
                    removedPlayers.Add(pl);
                    continue;
                }
            }
            for (int i = 0; i < removedPlayers.Count; i++)
            {
                game.RemovePlayer(removedPlayers[i]);
            }
            for (int i = 0; i < game.GetPlayerCount(); i++)
            {
                GameObject playerField = finalWagers.transform.GetChild(i).gameObject;
                playerField.SetActive(true);
                playerField.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = game.GetPlayer(i).GetName();

            }
            this.FinalJeopardyModPanel.SetActive(true);
            this.FinalJeopardyPlayerPanel.SetActive(true);
        }
    }

    public void ConfirmFinalWagers()
    {
        GameObject finalWagers = FinalJeopardyModPanel.transform.GetChild(0).gameObject;
        for (int i = 0; i< game.GetPlayerCount(); i++)
        {
            this.wagers.Add(int.Parse(finalWagers.transform.GetChild(i).GetChild(0).GetComponent<TMP_InputField>().text));
        }
        this.FinalJeopardyModPanel.SetActive(false);
        this.FinalJeopardyPlayerPanel.SetActive(false);
        FinalMod();
    }

    public void FinalMod() {
        Clue finalClue = game.GetFinalJeopardyClue();
        //setup final clue
        FinalClueModPanel.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Category: " + game.GetFinalJeopardyCat();
        FinalClueModPanel.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Answer: " + finalClue.GetAnswer();
        GameObject finalWagers = FinalClueModPanel.transform.GetChild(0).gameObject;
        for (int i = 0; i < game.GetPlayerCount(); i++)
        {
            MyPlayer pl = game.GetPlayer(i);
            GameObject playerField = finalWagers.transform.GetChild(i).gameObject;
            playerField.SetActive(true);
            playerField.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = pl.GetName();
            playerField.transform.GetChild(1).GetComponent<Text>().text = this.wagers[i].ToString() + " $";

        }
        this.FinalClueModPanel.SetActive(true);
        DisplayClue(-1, -1, null, true);
        //this.FinalCluePlayerPanel.SetActive(true);
    }

    public void FinalRate(bool isRight, int player)
    {
        RightWrongButtons[player].SetActive(false);
        if (isRight)
            game.GetPlayer(player).AddPoints(wagers[player]);
        else
            game.GetPlayer(player).AddPoints(-wagers[player]);
    }

    public void FinalRateRight(int player)
    {
        FinalRate(true, player);
    }
    public void FinalRateWrong(int player)
    {
        FinalRate(false, player);
    }
    public void DisplayWinner()
    {
        
        List<MyPlayer> players = game.GetPlayers();
        players.Sort();
        for(int i = 0; i< game.GetPlayerCount(); i++)
        {
            MyPlayer player = game.GetPlayer(i);
            GameObject playerScore = ScoresPlayerPanel.transform.GetChild(0).GetChild(i).gameObject;
            playerScore.SetActive(true);
            playerScore.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = player.GetName();
            playerScore.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "$" + player.GetPoints().ToString();

            GameObject playerModScore = ScoresModPanel.transform.GetChild(0).GetChild(i).gameObject;
            playerModScore.SetActive(true);
            playerModScore.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "#" + (i+1) + " " + player.GetName();
            playerModScore.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "$" + player.GetPoints().ToString();
        }
        for (int i = 0; i < active_Now.transform.childCount; i++)
            active_Now.transform.GetChild(i).gameObject.SetActive(false);
        //turn off old Panels
        FinalClueModPanel.SetActive(false);
        active_Now.SetActive(false);
        //turn on score panels
        ScoresPlayerPanel.SetActive(true);
        ScoresModPanel.SetActive(true);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void Play(GameObject videoObj)
    {
        videoObj.GetComponent<VideoPlayer>().Play();
    }

    public void Pause(GameObject videoObj)
    {
        videoObj.GetComponent<VideoPlayer>().Pause();
    }

    public void Rewind(GameObject videoObj)
    {
        videoObj.GetComponent<VideoPlayer>().time = 0;
    }
}