public class SingleClue : Clue
{
    string clue;

    public SingleClue(string clue, string answer, int difficulty) : base(answer,difficulty)
    {
        this.clue = clue;
    }

    public string GetClue()
    {
        return this.clue;
    }
    public override bool IsDual()
    {
        return false; 
    }
    public Clue_Type GetClueType()
    {
        if (clue.EndsWith(".mp4")) return Clue_Type.VIDEO;
        else if (clue.EndsWith(".png") || clue.EndsWith(".jpg")) return Clue_Type.IMAGE;
        else if (clue.EndsWith(".mp3")) return Clue_Type.AUDIO;
        else return Clue_Type.TEXT;
    }
    
}
