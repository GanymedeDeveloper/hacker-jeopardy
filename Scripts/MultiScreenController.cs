using UnityEngine;

public class MultiScreenController : MonoBehaviour
{

    public Camera mod_cam;
    public Camera pub_cam;
    public Canvas mod_canvas;
    public Canvas pub_canvas;
    void Start()
    {
        Init_Displays();
    }

    public void Screen_Switcher()
    {

        if (mod_cam.gameObject.active)
        {
            mod_cam.gameObject.SetActive(false);
            mod_canvas.sortingOrder = 0;

            pub_cam.gameObject.SetActive(true);
            pub_canvas.sortingOrder = 1;
        }
        else
        {
            pub_cam.gameObject.SetActive(false);
            pub_canvas.sortingOrder = 0;
            
            mod_cam.gameObject.SetActive(true);
            mod_canvas.sortingOrder = 1;

        }

    }

    public void Init_Displays()
    {
        for (int i = 0; i < Display.displays.Length; i++)
        {
            Display.displays[i].Activate(1920, 1080, 60);
        }
    }

}
