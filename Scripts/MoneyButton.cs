using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MoneyButton : MonoBehaviour, IPointerClickHandler
{
    public int category;
    public int index;
    Manager manager;
    private void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Manager>();
        if (!manager.game.IsDouble())
            this.GetComponent<Text>().text = "$ " + (100 * (index + 1));
        else
            this.GetComponent<Text>().text = "$ " + (200 * (index + 1));
    }
    public void OnPointerClick(PointerEventData eventData)
    {

        if (manager.moderator_Mode)
        {
            manager.DisplayClue(category, index, this.gameObject, false);
            GameObject.Find(this.gameObject.name).SetActive(false);
            this.gameObject.SetActive(false);
        }
    }
}
