public class DualClue : Clue
{
    string firstClue;
    string secoundClue; 

    public DualClue(string clue1,string clue2, string answer, int difficulty) :base (answer ,difficulty)
    {
        this.firstClue = clue1;
        this.secoundClue = clue2; 
    }
    public override bool IsDual()
    {
        return true;
    }

    public string GetClue1()
    {
     
        return this.firstClue;
    }
    public string GetClue2()
    {
        return this.secoundClue; 
    }
    public Clue_Type GetType1()
    {
        if (firstClue.EndsWith(".mp4")) return Clue_Type.VIDEO;
        else if (firstClue.EndsWith(".png") || firstClue.EndsWith(".jpg")) return Clue_Type.IMAGE;
        else if (firstClue.EndsWith(".mp3")) return Clue_Type.AUDIO;
        else return Clue_Type.TEXT;
    }
    public Clue_Type GetType2()
    {
        if (secoundClue.EndsWith(".mp4")) return Clue_Type.VIDEO;
        else if (secoundClue.EndsWith(".png") || secoundClue.EndsWith(".jpg")) return Clue_Type.IMAGE;
        else if (secoundClue.EndsWith(".mp3")) return Clue_Type.AUDIO;
        else return Clue_Type.TEXT;
    }
}
