using UnityEngine;

public class GameCreator: MonoBehaviour
{
    string gameName;
    
    Clue finaljeopardy;

   
    public string getName()
    {
        return this.gameName; 
    }

    public Clue getFinalJeopardy()
    {
        return this.finaljeopardy;
    }
  
    public void setName(string name)
    {
        this.gameName = name; 
    }

    public void setClue(Clue clue)
    {
        this.finaljeopardy = clue; 
    }

    
 
}
