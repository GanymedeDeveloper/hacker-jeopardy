using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityScript.Steps;

public class FileDragAndDrop : MonoBehaviour , IPointerEnterHandler , IPointerExitHandler
{
    private string file;
    public Text path;
    public Image img; 
    // important to keep the instance alive while the hook is active.
    UnityDragAndDropHook hook;

    void OnFiles(List<string> aFiles, Lint aPos)
    {
        


            // do something with the dropped file names. aPos will contain the 
            // mouse position within the window where the files has been dropped.
            //Debug.Log("Dropped " + aFiles.Count + " files at: " + aPos + "\n" +
            //    aFiles.Aggregate((a, b) => a + "\n" + b));
            file = aFiles.ElementAt(0);
            path.text = file;
        
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
    
     Color green = new Color(0, 1, 0, 1);
    Debug.Log("MouseEnter");
        hook = new UnityDragAndDropHook();
        hook.InstallHook();
        img.color = green;
        while (Input.GetMouseButtonDown(0))
        {

        }
        hook.OnDroppedFiles += OnFiles;
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        Color white = new Color(1, 1, 1, 1);
        hook.UninstallHook();
        img.color = white; 
        Debug.Log("MouseExit");
    }
}
